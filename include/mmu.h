#ifndef __MMU_H__

#define __MMU_H__

#define CR0_PG	0x80000000
#define CR0_WP	0x00010000

#define PAGE_SIZE	4096

#define PAGE_TABLE_KERNEL	0
#define PAGE_TABLE_USER		1
#define PAGE_TABLE_FREE		2

struct pde_s {
    int p:1;
    int rw:1;
    int us:1;
    int pwt: 1;
    int pcd:1;
    int a:1;
    int unused2:1;
    int ps:1;
    int unused1:4;
    int address:20;
} __attribute__((packed));

struct pte_s {
    int p:1;
    int rw:1;
    int us:1;
    int pwt: 1;
    int pcd:1;
    int a:1;
    int d:1;
    int pat:1;
    int g:1;
    int unused1:3;
    int address:20;
} __attribute__((packed));

#define PT_SIZE 1024

extern void setup_mmu();
extern unsigned int get_fault_address();
extern void flush_tlb();
extern void setup_page_table(int, struct pte_s[]);
#endif

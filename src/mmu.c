#include "minilib.h"
#include "tss.h"
#include "mmu.h"

static struct pde_s page_dir[PT_SIZE] __attribute__((aligned (PAGE_SIZE)));
static struct pte_s page_table_kernel[PT_SIZE] __attribute__((aligned (PAGE_SIZE)));
static struct pte_s page_table_user[PT_SIZE] __attribute__((aligned (PAGE_SIZE)));

void setup_page_table(int index, struct pte_s *ptable) {
	page_dir[index].ps = 0;
	page_dir[index].a = 0;
	page_dir[index].pcd = 0;
	page_dir[index].pwt = 0;
	page_dir[index].us = 1;
	page_dir[index].rw = 1;
	page_dir[index].p = 1;
	page_dir[index].address = ((int)ptable) >> 12;
}

void setup_mmu() {
	unsigned int i;

	for (i = 0; i < PT_SIZE; i++) {
		page_table_kernel[i].g = 0;
		page_table_kernel[i].pat = 0;
		page_table_kernel[i].d = 0;
		page_table_kernel[i].a = 0;
		page_table_kernel[i].pcd = 0;
		page_table_kernel[i].pwt = 0;
		page_table_kernel[i].us = 0;
		page_table_kernel[i].rw = 1;
		page_table_kernel[i].p = 1;
		page_table_kernel[i].address = i;
		page_table_kernel[i].unused1 = 0;
	}

	for (i = 0; i < PT_SIZE; i++) {
		page_table_user[i].g = 0;
		page_table_user[i].pat = 0;
		page_table_user[i].d = 0;
		page_table_user[i].a = 0;
		page_table_user[i].pcd = 0;
		page_table_user[i].pwt = 0;
		page_table_user[i].us = 1;
		page_table_user[i].rw = 1;
		page_table_user[i].p = 1;
		page_table_user[i].address = i + PT_SIZE;
		page_table_user[i].unused1 = 0;
	}


	setup_page_table(PAGE_TABLE_KERNEL, page_table_kernel);
	setup_page_table(PAGE_TABLE_USER, page_table_user);

	i = (int) page_dir;
	asm("movl %0, %%cr3"  : : "r"(i) :);

	asm("movl %%cr0, %0" : "=r"(i) : :);
	i |= CR0_PG | CR0_WP;
	asm("movl %0, %%cr0"  : : "r"(i) :);
}

unsigned int get_fault_address() {
	unsigned int v;
	asm("movl %%cr2, %0" : "=r"(v) : :);
	return v;
}

void flush_tlb() {
	unsigned int v;
	asm("movl %%cr3, %0" : "=r"(v) : :);
	asm("movl %0, %%cr3"  : : "r"(v) :);
}

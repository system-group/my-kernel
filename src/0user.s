global user

extern user_entry
extern user_stack

user:
	mov ax, 0x28
	ltr ax
	
	mov ax, 0x23
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov eax, user_stack + 65536 - 4
	push 0x23
	push eax
	pushf
	push 0x1B
	push user_entry
	iret


